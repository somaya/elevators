<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">اسم الفرع : </label>
    <div class="col-lg-10{{ $errors->has('name') ? ' has-danger' : '' }}">
        {!! Form::text('name',null,['class'=>'form-control m-input','autofocus','placeholder'=>"اسم الفرع"]) !!}
        @if ($errors->has('name'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>

</div>
<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">رقم الموبايل: </label>
    <div class="col-lg-10{{ $errors->has('phone') ? ' has-danger' : '' }}">
        {!! Form::text('phone',null,['class'=>'form-control m-input','autofocus','placeholder'=>"رقم الموبايل"]) !!}
        @if ($errors->has('phone'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('phone') }}</strong>
            </span>
        @endif
    </div>

</div>
<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label"> التفاصيل: </label>
    <div class="col-lg-10{{ $errors->has('details') ? ' has-danger' : '' }}">
        {!! Form::textarea('details',old('details'),['class'=>'form-control m-input ','autofocus','placeholder'=> 'تفاصيل الفرع ارقام التلفون ومواعيد العمل ' ]) !!}
        @if ($errors->has('details'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('details') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">العنوان(رابط الخريطه): </label>
    <div class="col-lg-10{{ $errors->has('address') ? ' has-danger' : '' }}">
        {!! Form::text('address',null,['class'=>'form-control m-input','autofocus','placeholder'=>"العنوان"]) !!}
        @if ($errors->has('address'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('address') }}</strong>
            </span>
        @endif
    </div>

</div>




