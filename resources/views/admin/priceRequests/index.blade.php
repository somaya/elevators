@extends('admin.layouts.app')
@section('title')
    طلبات عرض سعر
@endsection

@section('header')
    {!! Html::style('admin/vendors/custom/datatables/datatables.bundle.rtl.css') !!}
@endsection

@section('topBar')
    <li class="m-menu__item">
        <a href="{{url('/webadmin/dashboard')}}" class="m-menu__link">
            <span class="m-menu__link-text">الرئيسية</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="{{url('/webadmin/priceRequests')}}" class="m-menu__link">
            <span class="m-menu__link-text">طلبات عرض سعر</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>


@endsection

@section('content')
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        طلبات عرض سعر
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <br>
            {{--<div><a href="{{route('priceRequests.create')}}" style="margin-bottom:20px"--}}
                    {{--class="btn btn_primary btn btn-danger"><i class=" fa fa-edit"></i> اضف منتج</a></div>--}}
            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable search_result"
                   id="m_table_testArea">

                <thead>
                <tr>
                    <th>#</th>
                    <th>صاحب الطلب</th>
                    <th>الاسم بالكامل</th>
                    <th>رقم الجوال</th>
                    <th>العنوان</th>
                    <th>الاختيار</th>
                    <th>السعر</th>
                    <th>الاجراءات</th>
                </tr>
                </thead>
                <tbody>
                @foreach($priceRequests as $priceRequest)

                    <tr>
                        <td>{{$priceRequest->id}}</td>
                        <td><a href="/webadmin/users/{{$priceRequest->user_id}}">{{$priceRequest->user->name}}</a></td>
                        <td>{{$priceRequest->name}} </td>
                        <td>{{$priceRequest->phone}} </td>
                        <td>{{$priceRequest->address  }} </td>
                        <td>
                                <a href="/webadmin/choices/{{$priceRequest->choice_id}}">{{$priceRequest->choice_id}}</a><br>
                        </td>
                        <td>{{$priceRequest->price  }} </td>

                        <td>

                            <a  title="عرض التفاصيل" href="/webadmin/priceRequests/{{$priceRequest->id}}" ><i class="fa fa-eye"></i></a>
                            @if($priceRequest->answer==0)
                            <a  title="الرد بالسعر" href="/webadmin/priceAnswer/{{$priceRequest->id}}" ><i class="fa fa-reply"></i></a>
                            @endif
                            {{--<a title="Edit" href="/webadmin/priceRequests/{{$priceRequests->id}}/edit"><i--}}
                                    {{--class="fa fa-edit"></i></a>--}}
                            <form class="inline-form-style"
                                  action="/webadmin/priceRequests/{{ $priceRequest->id }}"
                                  method="post">
                                <button type="submit" class="trash-btn">
                                    <span class="fa fa-trash"></span>
                                </button>
                                <input type="hidden" name="_method" value="delete"/>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            </form>

                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('footer')
    {{--{!! Html::script('admin/vendors/custom/datatables/datatables.bundle.js') !!}--}}
    {{--{!! Html::script('admin/custom/js/testArea/script.js') !!}--}}

@endsection
