@extends('admin.layouts.app')

@section('title')
    الرد بالسعر
@endsection
@section('topBar')
    <li class="m-menu__item">
        <a href="{{url('/webadmin/dashboard')}}" class="m-menu__link">
            <span class="m-menu__link-text">الرئيسية</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="{{url('/webadmin/categories')}}" class="m-menu__link">
            <span class="m-menu__link-text"> طلبات عرض سعر</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item" style="background: #e0deff;">
        <a href="" class="m-menu__link">
            <span class="m-menu__link-text">الرد بالسعر</span>
            <i class="m-menu__hor-arrow la la-angle-down"></i>
        </a>
    </li>
@endsection

@section('header')
@endsection

@section('content')

    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
					</span>
                    <h3 class="m-portlet__head-text">
                        الرد بالسعر
                    </h3>
                </div>
            </div>
        </div>
        <!--begin::Form-->
        <form class="m-form m-form--fit m-form--label-align-right" action="/webadmin/priceAnswer/{{$priceRequest->id}}/store" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="m-portlet__body">
                <div class="form-group m-form__group row">
                    <label class="col-lg-2 col-form-label">السعر : </label>
                    <div class="col-lg-10{{ $errors->has('price') ? ' has-danger' : '' }}">
                        {!! Form::number('price',null,['class'=>'form-control m-input','autofocus','placeholder'=>"السعر"]) !!}
                        @if ($errors->has('price'))
                            <span class="form-control-feedback" role="alert">
                                <strong>{{ $errors->first('price') }}</strong>
                            </span>
                        @endif
                    </div>

                </div>
            </div>
            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m-form__actions--solid">
                    <div class="row">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-6">
                            <button type="submit" class="btn btn-success">اضافة </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!--end::Form-->
    </div>
    <!--end::Portlet-->
@endsection
@section('footer')
    <script type="text/javascript">

    </script>
@endsection

