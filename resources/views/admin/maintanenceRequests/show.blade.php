@extends('admin.layouts.app')

@section('title')
    تفاصيل طلب الصيانة
@endsection
@section('topBar')
    <li class="m-menu__item">
        <a href="{{url('/webadmin/dashboard')}}" class="m-menu__link">
            <span class="m-menu__link-text">الرئيسية</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="{{url('/webadmin/maintanenceRequests')}}" class="m-menu__link">
            <span class="m-menu__link-text">طلبات الصيانة</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="" class="m-menu__link">
            <span class="m-menu__link-text">تفاصيل طلب الصيانة</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
@endsection

@section('header')
@endsection

@section('content')
    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
					</span>
                    <h3 class="m-portlet__head-text">
                        تفاصيل طلب الصيانة
                    </h3>
                </div>
            </div>
        </div>
        <!--begin::Form-->
        {!! Form::model($maintanenceRequest,['route' => ['maintanenceRequests.show' , $maintanenceRequest->id],'method'=> 'get','class'=>'m-form m-form--fit m-form--label-align-right',"enctype"=>"multipart/form-data"]) !!}
        <div class="m-portlet__body">


            <div class="form-group m-form__group row">
                <label class="col-lg-1 col-form-label">الاسم بالكامل </label>
                <div class="col-lg-5{{ $errors->has('name') ? ' has-danger' : '' }}">
                    {!! Form::text('name',old('name'),['class'=>'form-control m-input','autofocus','disabled' ]) !!}

                </div>
                <label class="col-lg-1 col-form-label">رقم الجوال</label>
                <div class="col-lg-5{{ $errors->has('phone') ? ' has-danger' : '' }}">
                    {!! Form::text('phone',old('phone'),['class'=>'form-control m-input','disabled' ]) !!}

                </div>


            </div>


            <div class="form-group m-form__group row">
                <label class="col-lg-1 col-form-label">العنوان</label>
                <div class="col-lg-5{{ $errors->has('address') ? ' has-danger' : '' }}">
                    {!! Form::text('address',old('address'),['class'=>'form-control m-input','disabled' ]) !!}

                </div>
                <label class="col-lg-1 col-form-label">الفرع</label>
                <div class="col-lg-5{{ $errors->has('branch') ? ' has-danger' : '' }}">
                    <input type="text" class="form-control m-input" value="{{$maintanenceRequest->branch->name}}" disabled>

                </div>


            </div>
            <div class="form-group m-form__group row">
                <label class="col-lg-1 col-form-label">نوع المصعد</label>
                <div class="col-lg-3{{ $errors->has('elevator_type') ? ' has-danger' : '' }}">
                    {!! Form::text('elevator_type',old('elevator_type'),['class'=>'form-control m-input','disabled' ]) !!}

                </div>
                <label class="col-lg-1 col-form-label">عمر المصعد</label>
                <div class="col-lg-3{{ $errors->has('elevator_age') ? ' has-danger' : '' }}">
                    {!! Form::text('elevator_age',old('elevator_age'),['class'=>'form-control m-input','disabled' ]) !!}

                </div>
                <label class="col-lg-1 col-form-label">نوع الخدمه</label>
                <div class="col-lg-3{{ $errors->has('service_type') ? ' has-danger' : '' }}">
                    <input type="text" class="form-control m-input" value="{{$maintanenceRequest->service_type==1?'صيانة مصعد':($maintanenceRequest->service_type==2?'اصلاح عطل':'عقد صيانه')}}" disabled>

                </div>

            </div>
            <div class="form-group m-form__group row">
                <label class="col-lg-2 col-form-label">وصف الطلب</label>
                <div class="col-lg-10{{ $errors->has('details') ? ' has-danger' : '' }}">
                    {!! Form::textarea('details',old('details'),['class'=>'form-control m-input','disabled' ]) !!}

                </div>



            </div>








        </div>

    {!! Form::close() !!}
    <!--end::Form-->
    </div>
    <!--end::Portlet-->
@endsection
@section('footer')
    <script type="text/javascript">

    </script>
@endsection

