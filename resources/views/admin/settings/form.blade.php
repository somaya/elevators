<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label"> البريد الالكتروني </label>
    <div class="col-lg-4{{ $errors->has('email') ? ' has-danger' : '' }}">
        {!! Form::email('email',old('email'),['class'=>'form-control m-input','autofocus','placeholder'=> 'Email' ]) !!}
        @if ($errors->has('email'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>
    <label class="col-lg-2 col-form-label">الهاتف </label>
    <div class="col-lg-4{{ $errors->has('phone') ? ' has-danger' : '' }}">
        {!! Form::text('phone',old('phone'),['class'=>'form-control m-input','placeholder'=> 'Phone' ]) !!}
        @if ($errors->has('phone'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('phone') }}</strong>
            </span>
        @endif
    </div>

</div>


<div class="form-group m-form__group row">
    <label class="col-lg-1 col-form-label">الموقع</label>
    <div class="col-lg-3{{ $errors->has('website') ? ' has-danger' : '' }}">
        {!! Form::text('website',old('website'),['class'=>'form-control m-input','placeholder'=> 'الموقع' ]) !!}
        @if ($errors->has('website'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('website') }}</strong>
            </span>
        @endif
    </div>

    <label class="col-lg-1 col-form-label">فيسبوك</label>
    <div class="col-lg-3{{ $errors->has('facebook') ? ' has-danger' : '' }}">
        {!! Form::text('facebook',old('facebook'),['class'=>'form-control m-input','placeholder'=> 'فيسبوك' ]) !!}
        @if ($errors->has('facebook'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('facebook') }}</strong>
            </span>
        @endif
    </div>


    <label class="col-lg-1 col-form-label">تويتر</label>
    <div class="col-lg-3{{ $errors->has('twitter') ? ' has-danger' : '' }}">
        {!! Form::text('twitter',old('twitter'),['class'=>'form-control m-input','placeholder'=> 'تويتر' ]) !!}
        @if ($errors->has('twitter'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('twitter') }}</strong>
            </span>
        @endif
    </div>

</div>



<div class="form-group m-form__group row">
    <label class="col-lg-1 col-form-label">لوجو التطبيق</label>
    <div class="col-lg-11{{ $errors->has('logo') ? ' has-danger' : '' }}">

        <input type="file" name="logo" class="form-control uploadinput">
        @if ($errors->has('logo'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('logo') }}</strong>
            </span>
        @endif
    </div>

</div>
@if(isset($settings) && $settings->logo)
    <input type="hidden" value="{{ $settings->logo }}" name="logo">
    <div class="row">

        <div class="col-lg-6 col-md-6 col-sm-6" style="margin-bottom: 10px; text-align: center">

            <img
                data-src="holder.js/800x400?auto=yes&amp;bg=777&amp;fg=555&amp;text=First slide"
                alt="First slide [800x4a00]"
                src="{{asset($settings->logo)}}"
                style="height: 150px; width: 150px"
                data-holder-rendered="true">
        </div>

    </div>
@endif


<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label"> خدماتنا: </label>
    <div class="col-lg-10{{ $errors->has('our_services') ? ' has-danger' : '' }}">
        {!! Form::textarea('our_services',old('our_services'),['class'=>'form-control m-input summernote','autofocus','placeholder'=> 'خدماتنا' ]) !!}
        @if ($errors->has('our_services'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('our_services') }}</strong>
            </span>
        @endif
    </div>
</div>


<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label"> شروط التعاقد: </label>
    <div class="col-lg-10{{ $errors->has('contract_rules') ? ' has-danger' : '' }}">
        {!! Form::textarea('contract_rules',old('contract_rules'),['class'=>'form-control m-input summernote','autofocus','placeholder'=> 'شروط التعاقد' ]) !!}
        @if ($errors->has('contract_rules'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('contract_rules') }}</strong>
            </span>
        @endif
    </div>
</div>


