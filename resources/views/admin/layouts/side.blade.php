<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('/webadmin/dashboard')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-line-graph"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">الرئيسية</span>
            </span>
        </span>
    </a>
</li>

<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('/webadmin/settings/1/edit')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-settings"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">الاعدادات</span>
            </span>
        </span>
    </a>
</li>



<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('/webadmin/users')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-user"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">الاعضاء</span>
            </span>
        </span>
    </a>
</li>
<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('/webadmin/admins')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-users-1"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">المدراء</span>
            </span>
        </span>
    </a>
</li>
<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('/webadmin/categories')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-technology-1"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">الاقسام</span>
            </span>
        </span>
    </a>
</li>


<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('/webadmin/branches')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-add"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">الفروع</span>
            </span>
        </span>
    </a>
</li>
<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('/webadmin/advs')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-confetti"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">الاعلانات</span>
            </span>
        </span>
    </a>
</li>
<li class="m-menu__item" aria-haspopup="true">
    <a href="/webadmin/contacts" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-email"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">اتصل بنا</span>
            </span>
        </span>
    </a>
</li>


<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('/webadmin/products')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-shopping-basket"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">المنتجات</span>
            </span>
        </span>
    </a>
</li>
<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('/webadmin/maintanenceRequests')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-settings-1"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">طلبات الصيانه</span>
            </span>
        </span>
    </a>
</li>
<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('/webadmin/choices')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-cart"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">الاختيارات</span>
            </span>
        </span>
    </a>
</li>
<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('/webadmin/priceRequests')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-questions-circular-button"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">طلبات عرض سعر</span>
            </span>
        </span>
    </a>
</li>
<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('/webadmin/contracts')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-attachment"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">طلبات التعاقد</span>
            </span>
        </span>
    </a>
</li>
