@extends('admin.layouts.app')

@section('title')
    تفاصيل الاختيار
@endsection
@section('topBar')
    <li class="m-menu__item">
        <a href="{{url('/webadmin/dashboard')}}" class="m-menu__link">
            <span class="m-menu__link-text">الرئيسية</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="{{url('/webadmin/choices')}}" class="m-menu__link">
            <span class="m-menu__link-text">الاختيارات</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="" class="m-menu__link">
            <span class="m-menu__link-text">تفاصيل الاختيار</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
@endsection

@section('header')
@endsection

@section('content')
    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
					</span>
                    <h3 class="m-portlet__head-text">
                        تفاصيل الاختيار
                    </h3>
                </div>
            </div>
        </div>
        <!--begin::Form-->
        {!! Form::model($choice,['route' => ['choices.show' , $choice->id],'method'=> 'get','class'=>'m-form m-form--fit m-form--label-align-right',"enctype"=>"multipart/form-data"]) !!}
        <div class="m-portlet__body">


            <div class="form-group m-form__group row">
                <label class="col-lg-1 col-form-label">الاسم بالكامل </label>
                <div class="col-lg-5{{ $errors->has('name') ? ' has-danger' : '' }}">
                    {!! Form::text('name',old('name'),['class'=>'form-control m-input','autofocus','disabled' ]) !!}

                </div>
                <label class="col-lg-1 col-form-label">رقم الجوال</label>
                <div class="col-lg-5{{ $errors->has('phone') ? ' has-danger' : '' }}">
                    {!! Form::text('phone',old('phone'),['class'=>'form-control m-input','disabled' ]) !!}

                </div>


            </div>


            <div class="form-group m-form__group row">
                <label class="col-lg-1 col-form-label">تاريخ التعاقد</label>
                <div class="col-lg-5{{ $errors->has('contract_date') ? ' has-danger' : '' }}">
                    {!! Form::text('contract_date',old('contract_date'),['class'=>'form-control m-input','disabled' ]) !!}

                </div>
                <label class="col-lg-1 col-form-label">تاريخ السداد</label>
                <div class="col-lg-5{{ $errors->has('payment_date') ? ' has-danger' : '' }}">
                    <input type="text" class="form-control m-input" value="{{$choice->payment_date}}" disabled>

                </div>


            </div>
            <div class="form-group m-form__group row">

                <label class="col-lg-2 col-form-label">القسم</label>
                <div class="col-lg-10{{ $errors->has('service_type') ? ' has-danger' : '' }}">
                    <input type="text" class="form-control m-input" value="{{$choice->category->name}}" disabled>

                </div>

            </div>
            <div class="form-group m-form__group row">
                <label class="col-lg-2 col-form-label">المنتجات</label>
                <div class="col-lg-10{{ $errors->has('details') ? ' has-danger' : '' }}">
                    @foreach($choice->products as $product)
                        <a href="/webadmin/products/{{$product->id}}">{{$product->name}}</a><br>
                        @endforeach

                </div>



            </div>








        </div>

    {!! Form::close() !!}
    <!--end::Form-->
    </div>
    <!--end::Portlet-->
@endsection
@section('footer')
    <script type="text/javascript">

    </script>
@endsection

