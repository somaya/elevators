@extends('admin.layouts.app')
@section('title')
    الاختيارات
@endsection

@section('header')
    {!! Html::style('admin/vendors/custom/datatables/datatables.bundle.rtl.css') !!}
@endsection

@section('topBar')
    <li class="m-menu__item">
        <a href="{{url('/webadmin/dashboard')}}" class="m-menu__link">
            <span class="m-menu__link-text">الرئيسية</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="{{url('/webadmin/choices')}}" class="m-menu__link">
            <span class="m-menu__link-text">الاختيارات</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>


@endsection

@section('content')
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        الاختيارات
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <br>
            {{--<div><a href="{{route('choices.create')}}" style="margin-bottom:20px"--}}
                    {{--class="btn btn_primary btn btn-danger"><i class=" fa fa-edit"></i> اضف منتج</a></div>--}}
            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable search_result"
                   id="m_table_testArea">

                <thead>
                <tr>
                    <th>#</th>
                    <th>صاحب الاختيار</th>
                    <th>الاسم بالكامل</th>
                    <th>رقم الجوال</th>
                    <th>القسم</th>
                    <th>تاريخ التعاقد</th>
                    <th>تاريخ السداد</th>
                    <th>المنتجات</th>
                    <th>الاجراءات</th>
                </tr>
                </thead>
                <tbody>
                @foreach($choices as $choice)

                    <tr>
                        <td>{{$choice->id}}</td>
                        <td><a href="/webadmin/users/{{$choice->user_id}}">{{$choice->user->name}}</a></td>
                        <td>{{$choice->name}} </td>
                        <td>{{$choice->phone}} </td>
                        <td>{{$choice->category->name  }} </td>
                        <td>{{$choice->contract_date  }} </td>
                        <td>{{$choice->payment_date  }} </td>
                        <td>
                            @foreach($choice->products as $product)
                                <a href="/webadmin/products/{{$product->id}}">{{$product->name}}</a><br>
                                @endforeach
                        </td>
                        <td>

                            <a  title="عرض التفاصيل" href="/webadmin/choices/{{$choice->id}}" ><i class="fa fa-eye"></i></a>
                            {{--<a title="Edit" href="/webadmin/choices/{{$choices->id}}/edit"><i--}}
                                    {{--class="fa fa-edit"></i></a>--}}
                            <form class="inline-form-style"
                                  action="/webadmin/choices/{{ $choice->id }}"
                                  method="post">
                                <button type="submit" class="trash-btn">
                                    <span class="fa fa-trash"></span>
                                </button>
                                <input type="hidden" name="_method" value="delete"/>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            </form>

                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('footer')
    {{--{!! Html::script('admin/vendors/custom/datatables/datatables.bundle.js') !!}--}}
    {{--{!! Html::script('admin/custom/js/testArea/script.js') !!}--}}

@endsection
