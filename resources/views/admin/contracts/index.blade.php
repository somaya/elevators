@extends('admin.layouts.app')
@section('title')
    طلبات التعاقد
@endsection

@section('header')
    {!! Html::style('admin/vendors/custom/datatables/datatables.bundle.rtl.css') !!}
@endsection

@section('topBar')
    <li class="m-menu__item">
        <a href="{{url('/webadmin/dashboard')}}" class="m-menu__link">
            <span class="m-menu__link-text">الرئيسية</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="{{url('/webadmin/contracts')}}" class="m-menu__link">
            <span class="m-menu__link-text">طلبات التعاقد</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>


@endsection

@section('content')
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        طلبات التعاقد
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <br>
            {{--<div><a href="{{route('contracts.create')}}" style="margin-bottom:20px"--}}
                    {{--class="btn btn_primary btn btn-danger"><i class=" fa fa-edit"></i> اضف منتج</a></div>--}}
            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable search_result"
                   id="m_table_testArea">

                <thead>
                <tr>
                    <th>#</th>
                    <th>صاحب الطلب</th>
                    <th>الاسم بالكامل</th>
                    <th>رقم الجوال</th>
                    <th>العنوان</th>
                    <th>الرقم القومي</th>
                    <th>الاختيار</th>
                    <th>الاجراءات</th>
                </tr>
                </thead>
                <tbody>
                @foreach($contracts as $contract)

                    <tr>
                        <td>{{$contract->id}}</td>
                        <td><a href="/webadmin/users/{{$contract->user_id}}">{{$contract->user->name}}</a></td>
                        <td>{{$contract->name}} </td>
                        <td>{{$contract->phone}} </td>
                        <td>{{$contract->address  }} </td>
                        <td>{{$contract->national_number  }} </td>
                        <td>
                                <a href="/webadmin/choices/{{$contract->choice_id}}">{{$contract->choice_id}}</a><br>
                        </td>
                        <td>

                            <a  title="عرض التفاصيل" href="/webadmin/contracts/{{$contract->id}}" ><i class="fa fa-eye"></i></a>
                            {{--<a title="Edit" href="/webadmin/contracts/{{$contracts->id}}/edit"><i--}}
                                    {{--class="fa fa-edit"></i></a>--}}
                            <form class="inline-form-style"
                                  action="/webadmin/contracts/{{ $contract->id }}"
                                  method="post">
                                <button type="submit" class="trash-btn">
                                    <span class="fa fa-trash"></span>
                                </button>
                                <input type="hidden" name="_method" value="delete"/>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            </form>

                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('footer')
    {{--{!! Html::script('admin/vendors/custom/datatables/datatables.bundle.js') !!}--}}
    {{--{!! Html::script('admin/custom/js/testArea/script.js') !!}--}}

@endsection
