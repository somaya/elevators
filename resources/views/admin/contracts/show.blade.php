@extends('admin.layouts.app')

@section('title')
    تفاصيل طلب تعاقد
@endsection
@section('topBar')
    <li class="m-menu__item">
        <a href="{{url('/webadmin/dashboard')}}" class="m-menu__link">
            <span class="m-menu__link-text">الرئيسية</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="{{url('/webadmin/contracts')}}" class="m-menu__link">
            <span class="m-menu__link-text">طلبات تعاقد</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="" class="m-menu__link">
            <span class="m-menu__link-text">تفاصيل طلب تعاقد</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
@endsection

@section('header')
@endsection

@section('content')
    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
					</span>
                    <h3 class="m-portlet__head-text">
                        تفاصيل طلب تعاقد
                    </h3>
                </div>
            </div>
        </div>
        <!--begin::Form-->
        {!! Form::model($contract,['route' => ['contracts.show' , $contract->id],'method'=> 'get','class'=>'m-form m-form--fit m-form--label-align-right',"enctype"=>"multipart/form-data"]) !!}
        <div class="m-portlet__body">


            <div class="form-group m-form__group row">
                <label class="col-lg-1 col-form-label">الاسم بالكامل </label>
                <div class="col-lg-5{{ $errors->has('name') ? ' has-danger' : '' }}">
                    {!! Form::text('name',old('name'),['class'=>'form-control m-input','autofocus','disabled' ]) !!}

                </div>
                <label class="col-lg-1 col-form-label">رقم الجوال</label>
                <div class="col-lg-5{{ $errors->has('phone') ? ' has-danger' : '' }}">
                    {!! Form::text('phone',old('phone'),['class'=>'form-control m-input','disabled' ]) !!}

                </div>


            </div>

            <div class="form-group m-form__group row">

                <label class="col-lg-1 col-form-label">العنوان</label>
                <div class="col-lg-5{{ $errors->has('service_type') ? ' has-danger' : '' }}">
                    <input type="text" class="form-control m-input" value="{{$contract->address}}" disabled>

                </div>
                <label class="col-lg-1 col-form-label">الرقم القومى</label>
                <div class="col-lg-5{{ $errors->has('service_type') ? ' has-danger' : '' }}">
                    <input type="text" class="form-control m-input" value="{{$contract->national_number}}" disabled>

                </div>

            </div>

            <div class="form-group m-form__group row">
                <label class="col-lg-2 col-form-label">  منتجات الاختيار</label>
                <div class="col-lg-10{{ $errors->has('details') ? ' has-danger' : '' }}">
                    @foreach($contract->choice->products as $product)
                        <a href="/webadmin/products/{{$product->id}}">{{$product->name}}</a><br>
                        @endforeach

                </div>



            </div>








        </div>

    {!! Form::close() !!}
    <!--end::Form-->
    </div>
    <!--end::Portlet-->
@endsection
@section('footer')
    <script type="text/javascript">

    </script>
@endsection

