@extends('admin.layouts.app')

@section('title')
    تفاصيل المنتج
@endsection
@section('topBar')
    <li class="m-menu__item">
        <a href="{{url('/webadmin/dashboard')}}" class="m-menu__link">
            <span class="m-menu__link-text">الرئيسية</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="{{url('/webadmin/products')}}" class="m-menu__link">
            <span class="m-menu__link-text">طلبات الصيانة</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="" class="m-menu__link">
            <span class="m-menu__link-text">تفاصيل المنتج</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
@endsection

@section('header')
@endsection

@section('content')
    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
					</span>
                    <h3 class="m-portlet__head-text">
                        تفاصيل المنتج
                    </h3>
                </div>
            </div>
        </div>
        <!--begin::Form-->
        {!! Form::model($product,['route' => ['products.show' , $product->id],'method'=> 'get','class'=>'m-form m-form--fit m-form--label-align-right',"enctype"=>"multipart/form-data"]) !!}
        <div class="m-portlet__body">


            <div class="form-group m-form__group row">
                <label class="col-lg-1 col-form-label">اسم المنتج  </label>
                <div class="col-lg-5{{ $errors->has('name') ? ' has-danger' : '' }}">
                    {!! Form::text('name',old('name'),['class'=>'form-control m-input','autofocus','disabled' ]) !!}

                </div>
                <label class="col-lg-1 col-form-label">السعر</label>
                <div class="col-lg-5{{ $errors->has('price') ? ' has-danger' : '' }}">
                    {!! Form::text('price',old('price'),['class'=>'form-control m-input','disabled' ]) !!}

                </div>


            </div>


            <div class="form-group m-form__group row">

                <label class="col-lg-2 col-form-label">القسم</label>
                <div class="col-lg-10{{ $errors->has('category') ? ' has-danger' : '' }}">
                    <input type="text" class="form-control m-input" value="{{$product->category->name}}" disabled>

                </div>


            </div>
            @if(isset($product) && $product->photo)
                <div class="row">

                    <div class="col-lg-6 col-md-6 col-sm-6" style="margin-bottom: 10px;">

                        <img
                                data-src="holder.js/800x400?auto=yes&amp;bg=777&amp;fg=555&amp;text=First slide"
                                alt="First slide [800x4a00]"
                                src="{{$product->photo}}"
                                style="height: 150px; width: 150px"
                                data-holder-rendered="true">
                    </div>

                </div>
            @endif








        </div>

    {!! Form::close() !!}
    <!--end::Form-->
    </div>
    <!--end::Portlet-->
@endsection
@section('footer')
    <script type="text/javascript">

    </script>
@endsection

