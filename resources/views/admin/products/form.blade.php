<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">اسم المنتج : </label>
    <div class="col-lg-10{{ $errors->has('name') ? ' has-danger' : '' }}">
        {!! Form::text('name',null,['class'=>'form-control m-input','autofocus','placeholder'=>"اسم المنتج"]) !!}
        @if ($errors->has('name'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>

</div>
<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">سعر المنتج : </label>
    <div class="col-lg-10{{ $errors->has('price') ? ' has-danger' : '' }}">
        {!! Form::number('price',null,['class'=>'form-control m-input','autofocus','placeholder'=>"سعر المنتج"]) !!}
        @if ($errors->has('price'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('price') }}</strong>
            </span>
        @endif
    </div>

</div>
<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">القسم : </label>
    <div class="col-lg-10{{ $errors->has('category_id') ? ' has-danger' : '' }}">
{{--        {!! Form::number('price',null,['class'=>'form-control m-input','autofocus','placeholder'=>"سعر المنتج"]) !!}--}}
        <select name="category_id" class="form-control m-input" >
            <option value="">---اختر القسم---</option>
            @foreach($categories as $category)
                <option value="{{$category->id}}" {{isset($product) && $product->category_id==$category->id ?'selected':''}}>{{$category->name}}</option>
            @endforeach


        </select>
        @if ($errors->has('category_id'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('category_id') }}</strong>
            </span>
        @endif
    </div>

</div>

<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">الصوره: </label>
    <div class="col-lg-10{{ $errors->has('photo') ? ' has-danger' : '' }}">

        <input type="file" name="photo"   class="form-control uploadinput">
        @if ($errors->has('photo'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('photo') }}</strong>
            </span>
        @endif
    </div>

</div>
@if(isset($product) && $product->photo)
    <div class="row">




        <div class="col-lg-6 col-md-6 col-sm-6" style="margin-bottom: 10px;">

            <img
                    data-src="holder.js/800x400?auto=yes&amp;bg=777&amp;fg=555&amp;text=First slide"
                    alt="First slide [800x4a00]"
                    src="{{$product->photo}}"
                    style="height: 150px; width: 150px"
                    data-holder-rendered="true">
        </div>

    </div>
@endif




