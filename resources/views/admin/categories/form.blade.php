<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">اسم القسم : </label>
    <div class="col-lg-10{{ $errors->has('name') ? ' has-danger' : '' }}">
        {!! Form::text('name',null,['class'=>'form-control m-input','autofocus','placeholder'=>"اسم القسم"]) !!}
        @if ($errors->has('name'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>

</div>
<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">الايقونة: </label>
    <div class="col-lg-10{{ $errors->has('icon') ? ' has-danger' : '' }}">

        <input type="file" name="icon"   class="form-control uploadinput">
        @if ($errors->has('icon'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('icon') }}</strong>
            </span>
        @endif
    </div>

</div>
@if(isset($category) && $category->icon)
    <div class="row">




        <div class="col-lg-6 col-md-6 col-sm-6" style="margin-bottom: 10px;">

            <img
                    data-src="holder.js/800x400?auto=yes&amp;bg=777&amp;fg=555&amp;text=First slide"
                    alt="First slide [800x4a00]"
                    src="{{$category->icon}}"
                    style="height: 150px; width: 150px"
                    data-holder-rendered="true">
        </div>

    </div>
@endif




