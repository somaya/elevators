<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">رابط الاعلان : </label>
    <div class="col-lg-10{{ $errors->has('link') ? ' has-danger' : '' }}">
        {!! Form::text('link',null,['class'=>'form-control m-input','autofocus','placeholder'=>"رابط الاعلان"]) !!}
        @if ($errors->has('link'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('link') }}</strong>
            </span>
        @endif
    </div>

</div>
<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">الصوره: </label>
    <div class="col-lg-10{{ $errors->has('photo') ? ' has-danger' : '' }}">

        <input type="file" name="photo"   class="form-control uploadinput">
        @if ($errors->has('photo'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('photo') }}</strong>
            </span>
        @endif
    </div>

</div>
@if(isset($adv) && $adv->photo)
    <div class="row">




        <div class="col-lg-6 col-md-6 col-sm-6" style="margin-bottom: 10px;">

            <img
                    data-src="holder.js/800x400?auto=yes&amp;bg=777&amp;fg=555&amp;text=First slide"
                    alt="First slide [800x4a00]"
                    src="{{$adv->photo}}"
                    style="height: 150px; width: 150px"
                    data-holder-rendered="true">
        </div>

    </div>
@endif




