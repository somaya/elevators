@extends("admin.layouts.app")
@section('title')
    لوحة تحكم المصاعد
@endsection
@section('content')
    <div class="page-wrapper">

        <div class="content container-fluid">

            <!-- Page Header -->
            <div class="page-header">
                <div class="row">
                    <div class="col-sm-12">
                        <h3 class="page-title">اهلا!</h3>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item active">لوحة التحكم</li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- /Page Header -->

            <div class="row">
                <div class="col-xl-3 col-sm-6 col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="dash-widget-header">
										<span class="dash-widget-icon text-primary border-primary">
											<i class="fe fe-users"></i>
										</span>
                                <div class="dash-count">
                                    <h3>{{$users}}</h3>
                                </div>
                            </div>
                            <div class="dash-widget-info">
                                <a href="/webadmin/users"><h6 class="text-muted">الاعضاء</h6></a>
                                <div class="progress progress-sm">
                                    <div class="progress-bar bg-primary w-{{$users}}"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-sm-6 col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="dash-widget-header">
										<span class="dash-widget-icon text-success">
											<i class="fe fe-credit-card"></i>
										</span>
                                <div class="dash-count">
                                    <h3>{{$choices}}</h3>
                                </div>
                            </div>
                            <div class="dash-widget-info">

                                <a href="/webadmin/choices"> <h6 class="text-muted">الاختيارات</h6></a>
                                <div class="progress progress-sm">
                                    <div class="progress-bar bg-success w-{{$choices}}"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-sm-6 col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="dash-widget-header">
										<span class="dash-widget-icon text-danger border-danger">
											<i class="fe fe-money"></i>
										</span>
                                <div class="dash-count">
                                    <h3>{{$products}}</h3>
                                </div>
                            </div>
                            <div class="dash-widget-info">

                                <a href="/webadmin/products"> <h6 class="text-muted">المنتجات</h6></a>
                                <div class="progress progress-sm">
                                    <div class="progress-bar bg-danger w-{{$products}}"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-sm-6 col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="dash-widget-header">
										<span class="dash-widget-icon text-warning border-warning">
											<i class="fe fe-folder"></i>
										</span>
                                <div class="dash-count">
                                    <h3>{{$contacts}}</h3>
                                </div>
                            </div>
                            <div class="dash-widget-info">

                                <a href="/webadmin/contacts"><h6 class="text-muted">رسائل اتصل بنا</h6></a>
                                <div class="progress progress-sm">
                                    <div class="progress-bar bg-warning w-{{$contacts}}"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-sm-6 col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="dash-widget-header">
										<span class="dash-widget-icon text-warning border-warning">
											<i class="fe fe-folder"></i>
										</span>
                                <div class="dash-count">
                                    <h3>{{$contracts}}</h3>
                                </div>
                            </div>
                            <div class="dash-widget-info">

                                <a href="/webadmin/contracts">  <h6 class="text-muted">طلبات التعاقد</h6></a>
                                <div class="progress progress-sm">
                                    <div class="progress-bar bg-warning w-{{$contracts}}"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-sm-6 col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="dash-widget-header">
										<span class="dash-widget-icon text-warning border-warning">
											<i class="fe fe-folder"></i>
										</span>
                                <div class="dash-count">
                                    <h3>{{$maintanence}}</h3>
                                </div>
                            </div>
                            <div class="dash-widget-info">

                                <a href="/webadmin/maintanenceRequests"><h6 class="text-muted">طلبات الصيانه</h6></a>
                                <div class="progress progress-sm">
                                    <div class="progress-bar bg-warning w-{{$maintanence}}"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-sm-6 col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="dash-widget-header">
										<span class="dash-widget-icon text-warning border-warning">
											<i class="fe fe-folder"></i>
										</span>
                                <div class="dash-count">
                                    <h3>{{$priceRequests}}</h3>
                                </div>
                            </div>
                            <div class="dash-widget-info">

                                <a href="/webadmin/priceRequests"><h6 class="text-muted">طلبات عرض سعر</h6></a>
                                <div class="progress progress-sm">
                                    <div class="progress-bar bg-warning w-{{$priceRequests}}"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
@endsection
