<?php

return [


    'language_updated' => 'Language Changed Successfully',
    'contactus_name_required' => 'Name Required',
    'contactus_phone_required' => 'Phone Required',
    'contactus_phone_numeric' => 'Phone Must Be Number',
    'contactus_email_required' => 'Email Required',
    'contactus_email_invalid' => 'Email Invalid',
    'contactus_subject_required' => 'Subject Required',
    'contactus_message_required' => 'Message Required',
    'add_address_city_id_required' => 'city_id Required',
    'add_address_full_address_required' => 'full_address Required',
    'add_address_latitude_required' => 'latitude Required',
    'add_address_longitude_required' => 'longitude Required',
    'fix_order_phone_type_id_required' => 'phone_type_id Required',
    'fix_order_phone_model_id_required' => 'phone_model_id Required',
    'fix_order_phone_color_id_required' => 'phone_color_id Required',
    'fix_order_problem_id_required' => 'problem_id Required',
    'fix_order_address_id_required' => 'address_id Required',
    'fix_order_service_type_required' => 'service_type Required',
    'fix_order_problem_details_required' => 'problem_details Required',




];
