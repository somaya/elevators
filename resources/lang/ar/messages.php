<?php

return [



    'language_updated' => 'تم تغيير اللغه بنجاح',
    'contactus_name_required' => 'الاسم مطلوب',
    'contactus_phone_required' => 'رقم الموبايل مطلوب',
    'contactus_phone_numeric' => 'رقم الموبايل يجب ان يكون رقم',
    'contactus_email_required' => 'البريد الالكتروني مطلوب',
    'contactus_email_invalid' => 'البريد الالكتروني غير صالح',
    'contactus_subject_required' => 'الموضوع مطلوب',
    'contactus_message_required' => 'الرسالة مطلوب',
    'add_address_city_id_required' => 'المدينه مطلوبه',
    'add_address_full_address_required' => 'تفاصيل العنوان مطلوب',
    'add_address_latitude_required' => 'خط العرض مطلوب',
    'add_address_longitude_required' => 'خط الطول مطلوب',
    'fix_order_phone_type_id_required' => 'نوع الموبايل مطلوب',
    'fix_order_phone_model_id_required' => 'موديل الموبايل مطلوب',
    'fix_order_phone_color_id_required' => 'لون الموبايل مطلوب',
    'fix_order_problem_id_required' => 'نوع العطل مطلوب',
    'fix_order_address_id_required' => 'العنوان مطلوب',
    'fix_order_service_type_required' => 'نوع الخدمه مطلوب',
    'fix_order_problem_details_required' => 'تفاصيل العطل',









];
