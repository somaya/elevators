<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['namespace' => 'Api', 'middleware' => 'language'], function () {

    Route::get('/update_language/{lng}', 'HomeController@updateLanguage');
// lists
    Route::get('get_advs', 'HomeController@getAdvs');
    Route::get('get_categories', 'HomeController@getCategories');
    Route::get('get_branches', 'HomeController@getBranches');
    Route::get('get_settings', 'HomeController@getSettings');
    Route::post('contact_us', 'HomeController@contactUs');


    // user
    Route::post('register', 'UserController@register');
    Route::post('verify_code', 'UserController@verifyUser');
    Route::post('user_resend_code', 'UserController@resendUserActivationCode');

    Route::get('get_profile', 'UserController@getProfile');
    Route::post('update_profile', 'UserController@updateProfile');
    Route::post('update_profile_image', 'UserController@changeProfilePicture');

    //products
    Route::get('get_products/{category}', 'HomeController@getProducts');
    Route::post('add_choice', 'ProductsController@addChoice');
    Route::post('maintenance_request', 'ProductsController@maintenanceRequest');
    Route::post('price_request', 'ProductsController@priceRequest');
    Route::post('contract_request', 'ProductsController@contractRequest');
    Route::get('get_my_choices', 'ProductsController@getMyChoices');
    Route::get('delete_choice/{id}', 'ProductsController@deleteChoice');

    // notifications
    Route::get('get_notifications', 'UserController@getNotifications');
    Route::get('/get_unread_notifications', 'UserController@unreadNotifications');
    Route::get('/delete_notifications', 'UserController@deleteNotification');
    Route::get('/delete_one_notification/{id}', 'UserController@deleteOneNotification');

});


