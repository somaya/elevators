<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Input;

Route::get('/', function () {

    return view('welcome');
});


Auth::routes();

Route::group(['prefix' => 'webadmin', 'middleware' => ['webadmin']], function () {
    Route::get('/dashboard', 'admin\DashboardController@index');
    Route::get('/logout', 'Auth\LoginController@logout');
    Route::resource('/users', 'admin\UsersController');
    Route::resource('/admins', 'admin\AdminsController');
    Route::resource('/contacts', 'admin\ContactsController');
    Route::get('/contact/{id}/reply', 'admin\ContactsController@reply');
    Route::post('/contact/{id}/sendreply', 'admin\ContactsController@sendReply');
    Route::resource('/settings', 'admin\SettingController');
    Route::resource('/categories', 'admin\CategoriesController');
    Route::resource('/branches', 'admin\BranchesController');
    Route::resource('/advs', 'admin\AdvsController');
    Route::resource('/products', 'admin\ProductsController');
    Route::resource('/maintanenceRequests', 'admin\MaintanenceRequestsController');
    Route::resource('/choices', 'admin\ChoicesController');
    Route::resource('/priceRequests', 'admin\PriceRequestsController');
    Route::resource('/contracts', 'admin\ContractsController');
    Route::get('/priceAnswer/{id}', 'admin\PriceRequestsController@price_answer');
    Route::post('/priceAnswer/{id}/store', 'admin\PriceRequestsController@send_price');



});
Route::get('/getStates', function () {
    $city_id = Input::get('city_id');
    $states = \App\Models\State::where('city_id', '=', $city_id)->get();
    return json_encode($states);

});
