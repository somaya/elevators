<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('phone');
            $table->string('photo')->nullable();
            $table->tinyInteger('role')->default(2);//1 for admin,  2 for user
            $table->string('email')->nullable()->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->string('tokens')->nullable();
            $table->string('device_token')->nullable();
            $table->string('address')->nullable();
            $table->string('login_code')->nullable();
            $table->timestamp('code_expire_at')->nullable();


//            $table->unsignedInteger('city_id')->nullable();
//            $table->foreign('city_id')
//                ->references('id')->on('cities')
//                ->onDelete("cascade")
//                ->onUpdate("cascade");
//
//            $table->unsignedInteger('state_id')->nullable();
//            $table->foreign('state_id')
//                ->references('id')->on('states')
//                ->onDelete("cascade")
//                ->onUpdate("cascade");
            $table->softDeletes();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
