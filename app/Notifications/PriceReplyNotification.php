<?php

namespace App\Notifications;

use App\Models\FixOrder;
use App\Models\PriceRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;

class PriceReplyNotification extends Notification
{
    use Queueable;

    protected $priceRequest;

    public function __construct(PriceRequest $priceRequest)
    {
        $this->priceRequest = $priceRequest;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function ToDatabase($notifiable)
    {
        return [
            'id' => $this->priceRequest->id,
            'image' => $this->priceRequest->choice->products[0]->image,
            'name' => 'الرد على طلب عرض سعر',
            'data' => $this->priceRequest->price  ,
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
