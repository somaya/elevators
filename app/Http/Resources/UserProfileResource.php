<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class  UserProfileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            "id" => (int)$this->id,
            "name" => (string)$this->name,
            "email" => (string)$this->email,
            "image" => $this->photo ? \Helpers::base_url() . '/' . $this->photo : '',
            "phone" => (string)$this->phone,
            "address" => (string)$this->address,
        ];

    }
}
