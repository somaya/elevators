<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Product extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'category' => $this->category->name,
            'category_id' => $this->category_id,
            'photo' => \Helpers::base_url().'/' . $this->photo,
            'price' => $this->price,
            'name' => $this->name,

        ];
    }
}
