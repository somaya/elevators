<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Setting extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'logo' => \Helpers::base_url().'/' . $this->logo,
            'email' => $this->email,
            'phone' => $this->phone,
            'facebook' => $this->facebook,
            'website' => $this->website,
            'twitter' => $this->twitter,
            'whatsapp' => $this->whatsapp,
            'our_services' => $this->our_services,
            'contact_rules' => $this->contact_rules,
        ];
    }
}
