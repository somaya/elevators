<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class ContactUs extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "email" => "required|email",
            "phone" => "required|numeric",
            "message" => "required",
        ];
    }
    protected function failedValidation (Validator $validator)
    {
        throw new HttpResponseException(response()->json(['error' => $validator->errors()->first()], 400));
    }
    public function messages ()
    {
        return [
            "email.required" => trans('messages.contactus_email_required'),
            "email.email" => trans('messages.contactus_email_invalid'),
            "phone.required" => trans('messages.contactus_phone_required'),
            "phone.numeric" => trans('messages.contactus_phone_numeric'),
            "message.required" => trans('messages.contactus_message_required'),
        ];
    }
}
