<?php

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use LaravelFCM\Facades\FCM;

class Helpers
{

    public static function base_url()
    {
        return URL::to('/');
    }


    public static function the_image($one)
    {
        if ($one->image) {
            return url($one->image->path);
        } else {
            return 'https://via.placeholder.com/700x300.png';
        }
    }
//    public static function send_sms($messageContent, $mobileNumber)
//    {
//        $user = 'eskanalmasiah';
//        $password = 'Qwer2234';
//        $sendername = 'elevators';
//        $text = urlencode($messageContent);
//        $to = $mobileNumber;
//// auth call
//        $url = "http://www.oursms.net/api/sendsms.php?username=$user&password=$password&numbers=$to&message=$text&sender=$sendername&unicode=E&return=full";
//
////لارجاع القيمه json
////$url = "http://www.oursms.net/api/sendsms.php?username=$user&password=$password&numbers=$to&message=$text&sender=$sendername&unicode=E&return=json";
//// لارجاع القيمه xml
////$url = "http://www.oursms.net/api/sendsms.php?username=$user&password=$password&numbers=$to&message=$text&sender=$sendername&unicode=E&return=xml";
//// لارجاع القيمه string
////$url = "http://www.oursms.net/api/sendsms.php?username=$user&password=$password&numbers=$to&message=$text&sender=$sendername&unicode=E";
//// Call API and get return message
////fopen($url,"r");
//        $ret = file_get_contents($url);
////        echo nl2br($ret);
//        return $ret;
//    }
    public static function send_sms($message,$mobile)
    {
        $username = 'Eskanalmasiah';
        $password = 'Qwer2234';
        $senderName = 'E.Almasiah';
        $message = urlencode($message);

        $url = "http://www.shamelsms.net/api/httpSms.aspx?username=$username&password=$password&mobile=$mobile&message=$message&sender=$senderName&unicodetype=U";


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL,$url);
        $content = curl_exec($ch);
        // echo $content;

        // $ret = file_get_contents($url);
        return $content;

    }
    public static function the_image_sm($one)
    {
        if ($one->image) {
            return url($one->image->path);
        } else {
            return 'https://via.placeholder.com/70x70.png';
        }
    }

    public static function failFindId()
    {
        if (request()->header('Accept-Language') == 'ar') {
            $message = 'لا يوجد نتائج ';
        } else {
            $message = 'No results for this id';
        }

        return $message;
    }


    /**
     * @param $lat1
     * @param $lon1
     * @param $lat2
     * @param $lon2
     * @param $unit
     * @return float|int
     */
    public static function distance($lat1, $lon1, $lat2, $lon2, $unit)
    {
        if (($lat1 == $lat2) && ($lon1 == $lon2)) {
            return 0;
        } else {
            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
            $unit = strtoupper($unit);

            if ($unit == "K") {
                return ($miles * 1.609344);
            } else if ($unit == "N") {
                return ($miles * 0.8684);
            } else {
                return $miles;
            }
        }
    }


    /**
     * @return string
     */
    public static function generateRandomString()
    {
        return Str::random(255);

    }

    /**
     * @param Request $request
     * @return array|string|null
     */
    public static function CheckAuthorizedRequest()
    {
        return \request()->header('Access-Token');
    }

    /**
     * @return mixed
     */
    public static function getLoggedUser()
    {
        $user = User:: where('tokens', Helpers::CheckAuthorizedRequest())->first();
        if ($user)
            return $user;
        else
            return 'No results';
    }

    /**
     * @param $user
     * @return bool
     */
    public static function updateFCMToken($user)
    {
        $fcm_token = \request()->header('fcm-token');
        $user->update([
            'device_token' => $fcm_token ? $fcm_token : null
        ]);
        return true;
    }

    /**
     * @param $status
     * @return string
     */
    public static function getOrderStatus($status)
    {
        if ($status == 'Pending')
            $status = 'قيد الانتظار';
        elseif ($status == 'Active')
            $status = 'نشط';
        elseif ($status == 'Completed')
            $status = 'منتهي';
        elseif ($status == 'Canceled')
            $status = 'ملغي';

        return $status;
    }


    /**
     * @param $token
     * @param $title
     * @param $message
     */
    public static function Firebase_notifications_fcm($token, $title, $message)
    {
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60 * 20);

        $notificationBuilder = new PayloadNotificationBuilder($title);
        $notificationBuilder->setBody($message)
            ->setSound('default');

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['click_action' => 'FLUTTER_NOTIFICATION_CLICK']);
        $dataBuilder->setData(['click_action' => 'FLUTTER_NOTIFICATION_CLICK']);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);

        $downstreamResponse->numberSuccess();
        $downstreamResponse->numberFailure();
        $downstreamResponse->numberModification();

// return Array - you must remove all this tokens in your database
        $downstreamResponse->tokensToDelete();

// return Array (key : oldToken, value : new token - you must change the token in your database)
        $downstreamResponse->tokensToModify();

// return Array - you should try to resend the message to the tokens in the array
        $downstreamResponse->tokensToRetry();

// return Array (key:token, value:error) - in production you should remove from your database the tokens
        $downstreamResponse->tokensWithError();
    }


    /**
     * @param $token
     * @param $message
     * @return bool
     */
    public static function fcm_notification($token, $content, $title, $message)
    {
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        $notification = [
            'notification' => $message,
            'sound' => true,
            'title' => $title,
            'body' => $content,
            'priority' => 'high',
        ];

        $extraNotificationData = ["data" => $notification];

        $fcmNotification = [
            'to' => $token, //single token
            'notification' => $notification,
            'data' => $extraNotificationData
        ];

        $headers = [
            'Authorization: key=AAAAJ-HHEfo:APA91bGL1Iev8KMySErsEvlPtNFg3y2tay6O-v8JJpLOd7ud2vk3zCQ6Oq2-FGOkp4nMThGeptFJsJ4zypUkAE3ZuAOjhgyJ-s8KDjfwraXx1_PZCgMh_oH2GoXRosVp5QgtjvP_o8Rg',
            'Content-Type: application/json'
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        curl_close($ch);
        return true;
    }

}
