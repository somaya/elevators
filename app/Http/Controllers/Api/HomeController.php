<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ContactUs;
use App\Http\Resources\AdvCollection;
use App\Http\Resources\BranchCollection;
use App\Http\Resources\CategoryCollection;
use App\Http\Resources\CityCollection;
use App\Http\Resources\HomeResource;
use App\Http\Resources\PhoneColorCollection;
use App\Http\Resources\PhoneModelCollection;
use App\Http\Resources\PhoneTypeCollection;
use App\Http\Resources\ProblemCollection;
use App\Http\Resources\ProductCollection;
use App\Http\Resources\Setting as SettingResource;
use App\Http\Resources\StateCollection;
use App\Models\Adv;
use App\Models\Branch;
use App\Models\Category;
use App\Models\City;
use App\Models\Contact;
use App\Models\PhoneColor;
use App\Models\PhoneModel;
use App\Models\PhoneTypes;
use App\Models\Problem;
use App\Models\Product;
use App\Models\Setting;
use App\Models\State;
use Helpers;

class HomeController extends Controller
{
    /**
     * @param $lng
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateLanguage($lng)
    {

        $message = session(['locale' => $lng]);
        return response()->json($message, 204);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCategories()
    {
        $categories = Category::all();
        return response()->json(new CategoryCollection($categories), 200);

    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAdvs()
    {
        $advs = Adv::all();
        return response()->json(new AdvCollection($advs), 200);

    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBranches()
    {
        $branches = Branch::all();
        return response()->json(new BranchCollection($branches), 200);

    }
    public function getProducts($id)
    {
        $category = Category::find($id);
        if (!$category) {
            return response()->json(['error' => Helpers::failFindId()], 400);
        }
        $products = Product::where('category_id',$id)->orderBy('created_at', 'desc')->get();
        return response()->json(new ProductCollection($products), 200);
//        return response()->json(\App\Http\Resources\Product::collection($products)->response()->getData(true), 200);
//
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSettings()
    {
        $settings = Setting::first();
        return response()->json(new SettingResource($settings), 200);

    }



    /**
     * @param ContactUs $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function contactUs(ContactUs $request)
    {
        $user = Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => Helpers::failFindId()], 400);
        }

        Contact::create([
            'user_id'=>$user->id,
            'phone'=>$request->phone,
            'email'=>$request->email,
            'message'=>$request->message
        ]);
        return response()->json([], 204);

    }



}


