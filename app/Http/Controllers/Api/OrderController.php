<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\FixOrderRequest;
use App\Http\Requests\RateRequest;
use App\Http\Resources\Order as OrderResource;
use App\Http\Resources\OrderCollection as OrderResourceCollection;
use App\Http\Resources\Transaction as TransactionResource;
use App\Http\Resources\TransactionCollection as TransactionResourceCollection;
use App\Models\FixOrder;
use App\Models\Problem;
use App\Models\Rating;
use App\Models\Setting;
use App\Models\Transaction;
use App\Notifications\AcceptOrderNotification;
use App\Notifications\NewOrderNotification;
use App\Notifications\TrackOrderNotification;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class OrderController extends Controller
{
    /**
     * @param FixOrderRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addFixOrder(FixOrderRequest $request)
    {
        $user = \Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }

        $problem = Problem::find($request->problem_id);
        if (!$problem) {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }

        $order = FixOrder::create([
            'user_id' => $user->id,
            'phone_type_id' => $request->phone_type_id,
            'phone_model_id' => $request->phone_model_id,
            'phone_color_id' => $request->phone_color_id,
            'problem_id' => $request->problem_id,
            'address_id' => $request->address_id,
            'service_type' => $request->service_type,
            'problem_details' => $request->problem_details,
            'service_price' => $problem->service_price,
            'price' => $problem->price,
            'order_number' => hexdec(uniqid()),
        ]);


        // engineers in the same place

        $engineers = User::where('city_id', $order->address->city_id)->where('type', 2)->get();

        foreach ($engineers as $engineer) {
            //notification
            $title = 'مصدر الذكيه';
            $content = 'طلب وارد';
            $message = [
                'id' => $order->id,
                "order_number" => $order->order_number,
                "type" => 'new_order',
            ];
            Notification::send($engineer, new NewOrderNotification($order));
            \Helpers::fcm_notification($engineer->device_token, $content, $title, $message);
        }
        return response()->json([], 204);
    }

    /**
     * @param RateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function rateOrder(RateRequest $request)
    {
        $user = \Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }

        $order = FixOrder::find($request->order_id);
        if (!$order) {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }

        Rating::create([
            'user_id' => $user->id,
            'order_id' => $order->id,
            'expression' => $request->expression,
            'degree' => $request->degree,
            'comment' => $request->comment,

        ]);
        return response()->json([], 204);
    }

    /**
     * @param $status
     */
    public function getOrders($status)
    {
        $user = \Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }
        if ($user->type == 1)
            $orders = FixOrder::where('status', $status)->where('user_id', $user->id)->get();
        else {
            if ($status == 'Pending') {
                $orders = FixOrder::where('status', $status)->whereHas('address', function ($q) use ($user) {
                    $q->where('city_id', $user->city_id);
                })->get();
            } else
                $orders = FixOrder::where('status', $status)->where('engineer_id', $user->id)->get();
        }
        if (!$orders) {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }
        return response()->json(new OrderResourceCollection($orders), 200);
    }

    /**
     * @param $status
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTransactions($status)
    {
        $user = \Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }
        if ($status == 'all')
            $transactions = Transaction::where('user_id', $user->id)->get();
        if ($status == 'dept')
            $transactions = Transaction::where('paid', 0)->where('user_id', $user->id)->get();
        if ($status == 'paid')
            $transactions = Transaction::where('paid', 1)->where('user_id', $user->id)->get();

        return response()->json(new TransactionResourceCollection($transactions), 200);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOrderDetails($id)
    {
        $user = \Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }
        $order = FixOrder::find($id);

        if (!$order) {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }
        return response()->json(new OrderResource($order), 200);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTransactionDetails($id)
    {
        $user = \Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }
        $transaction = Transaction::find($id);
        if (!$transaction) {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }
        return response()->json(new TransactionResource($transaction), 200);
    }


    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function cancelOrder($id)
    {
        $user = \Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }
        $order = FixOrder::find($id);
        if (!$order) {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }
        $order->update([
            'status' => 'Canceled'
        ]);

        return response()->json([], 204);
    }


    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function acceptOrder($id)
    {
        $user = \Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }
        $order = FixOrder::find($id);
        if (!$order) {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }
        $order->update([
            'engineer_id' => $user->id,
            'status' => 'Active',
            'tracking' => 2
        ]);

        //notification
        $user = User::find($order->user_id);
        $title = 'فيكس ستور';
        $content = 'تم قبول طلبكم';
        $message = $order->engineer->name . 'تم قبوله بواسطة المهندس' . $order->order_number . "الطلب رقم";
        Notification::send($user, new AcceptOrderNotification($order));
        \Helpers::fcm_notification($user->device_token, $content, $title, $message);

        return response()->json([], 204);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changeOrderTrack(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'order_id' => 'required',
            'tracking' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 400);
        }

        $order = FixOrder::find($request->order_id);
        $setting = Setting::first();
        if (!$order) {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }
        $order->update([
            'tracking' => $request->tracking
        ]);

        if ($request->tracking == 5) {
            $order->update([
                'status' => 'Completed'
            ]);
            Transaction::create([
                'user_id' => $order->engineer_id,
                'order_id' => $order->id,
                'amount' => $setting->fix_app_fees
            ]);
        }

        //notification
        $user = User::find($order->user_id);
        $title = 'فيكس ستور';
        $content = 'تم تحديث حالة الطلب';
        $message = $order->status . 'حالته' . $order->order_number . "الطلب رقم";
        Notification::send($user, new TrackOrderNotification($order));
        \Helpers::fcm_notification($user->device_token, $content, $title, $message);

        return response()->json([], 204);
    }
}
