<?php

namespace App\Http\Controllers\admin;

use App\Models\PriceRequest;
use App\Notifications\PriceReplyNotification;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Notification;

class PriceRequestsController extends Controller
{
    public function index()
    {
        $priceRequests=PriceRequest::orderBy('created_at','ASC')->paginate(10);
        return view('admin.priceRequests.index',compact('priceRequests'));
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $priceRequest=PriceRequest::find($id);
        return view('admin.priceRequests.show',compact('priceRequest'));
    }
    public function price_answer($id)
    {
        $priceRequest=PriceRequest::find($id);
        return view('admin.priceRequests.reply',compact('priceRequest'));
    }
    public function send_price(Request $request,$id)
    {
        $request->validate([

            'price' => 'required',
        ]);
        $priceRequest=PriceRequest::find($id);
        $priceRequest->update([
            'answer'=>1,
            'price'=>$request->price,
            ]);

        //notification
        $user = User::find($priceRequest->user_id);
        $title = ' المصاعد';
        $content = 'الرد على طلب عرض السعر';
        $message = [
            'id' => $priceRequest->id,
            "price" => $request->price,

        ];
        Notification::send($user, new PriceReplyNotification($priceRequest));
        \Helpers::fcm_notification($user->device_token, $content, $title, $message);
        return redirect('/webadmin/priceRequests')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم الرد بالسعر بنجاح']));


    }





    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PriceRequest::destroy($id);
        return redirect('/webadmin/priceRequests')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم حذف طلب عرض سعر بنجاح']));

    }
}
