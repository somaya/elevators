<?php

namespace App\Http\Controllers\admin;

use App\Models\ContractRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContractsController extends Controller
{
    public function index()
    {
        $contracts=ContractRequest::orderBy('created_at','ASC')->paginate(10);
        return view('admin.contracts.index',compact('contracts'));
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $contract=ContractRequest::find($id);
        return view('admin.contracts.show',compact('contract'));
    }





    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ContractRequest::destroy($id);
        return redirect('/webadmin/contracts')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم حذف طلب تعاقد بنجاح']));

    }
}
