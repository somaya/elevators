<?php

namespace App\Http\Controllers\admin;

use App\Models\Choice;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ChoicesController extends Controller
{
    public function index()
    {
        $choices=Choice::orderBy('created_at','ASC')->paginate(10);
        return view('admin.choices.index',compact('choices'));
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $choice=Choice::find($id);
        return view('admin.choices.show',compact('choice'));
    }





    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Choice::destroy($id);
        return redirect('/webadmin/choices')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم حذف الاختيار بنجاح']));

    }
}
