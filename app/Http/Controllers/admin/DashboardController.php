<?php

namespace App\Http\Controllers\admin;


use App\Models\Choice;
use App\Models\Company;
use App\Models\Contact;
use App\Models\ContractRequest;
use App\Models\Maintenance;
use App\Models\Meeting;
use App\Models\Offer;
use App\Models\PriceRequest;
use App\Models\Product;
use App\Models\RequestService;
use App\Models\Startup;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index(){

        $users=User::where('role','<>',1)->count();
        $products=Product::count();
        $choices=Choice::count();
        $contacts=Contact::count();
        $contracts=ContractRequest::count();
        $maintanence=Maintenance::count();
        $priceRequests=PriceRequest::count();

        return view('admin.index',compact('users','products','maintanence','contacts','contracts','priceRequests','choices'));
    }
}
