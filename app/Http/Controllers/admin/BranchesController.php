<?php

namespace App\Http\Controllers\admin;

use App\Models\Branch;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BranchesController extends Controller
{
    public function index()
    {
        $branches=Branch::orderBy('created_at','ASC')->paginate(10);
        return view('admin.branches.index',compact('branches'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.branches.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([

            'name' => 'required',
            'phone' => [
                'required',
                'regex:/^(05|5)([0-9]{8})$/',
            ],
            'address' => 'required',
            'details' => 'required',
        ]);
        Branch::create($request->all());

        return redirect('/webadmin/branches')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم اضافة الفرع بنجاح']));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $branch=Branch::find($id);
        return view('admin.branches.edit',compact('branch'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([

            'name' => 'required',
            'phone' => [
                'required',
                'regex:/^(05|5)([0-9]{8})$/',
            ],
            'address' => 'required',
            'details' => 'required',
        ]);
        $branch=Branch::find($id);
        $branch->update($request->all());

        return redirect('/webadmin/branches')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم تعديل الفرع بنجاح']));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Branch::destroy($id);
        return redirect('/webadmin/branches')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم حذف الفرع بنجاح']));

    }
}
