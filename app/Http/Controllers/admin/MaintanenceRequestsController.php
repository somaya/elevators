<?php

namespace App\Http\Controllers\admin;

use App\Models\Maintenance;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MaintanenceRequestsController extends Controller
{
    public function index()
    {
        $maintanenceRequests=Maintenance::orderBy('created_at','ASC')->paginate(10);
        return view('admin.maintanenceRequests.index',compact('maintanenceRequests'));
    }

   

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $maintanenceRequest=Maintenance::find($id);
        return view('admin.maintanenceRequests.show',compact('maintanenceRequest'));
    }

   

   

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Maintenance::destroy($id);
        return redirect('/webadmin/maintanenceRequests')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم حذف الطلب بنجاح']));

    }
}
