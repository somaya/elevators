<?php

namespace App\Http\Controllers\admin;

use App\Models\Adv;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdvsController extends Controller
{
    public function index()
    {
        $advs=Adv::orderBy('created_at','ASC')->paginate(10);
        return view('admin.advs.index',compact('advs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.advs.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([

            'link' => 'required',
        ]);
        $adv= Adv::create($request->except('photo'));

        if ($request->hasFile('photo')) {
            $request->validate([

                'photo' => 'image|mimes:jpeg,png,jpg,gif,svg'

            ]);

            $imageName = str_random(10) . '.' . $request->file('photo')->extension();
            $request->file('photo')->move(
                base_path() . '/public/uploads/advs/', $imageName
            );
            $adv->photo = '/uploads/advs/' . $imageName;
            $adv->save();

        }
        return redirect('/webadmin/advs')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم اضافة الاعلان بنجاح']));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $adv=Adv::find($id);
        return view('admin.advs.edit',compact('adv'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([

            'link' => 'required',
        ]);
        $adv=Adv::find($id);
        $adv->update($request->except('photo'));

        if ($request->hasFile('photo')) {
            $request->validate([

                'photo' => 'image|mimes:jpeg,png,jpg,gif,svg'

            ]);

            $imageName = str_random(10) . '.' . $request->file('photo')->extension();
            $request->file('photo')->move(
                base_path() . '/public/uploads/advs/', $imageName
            );
            $adv->photo = '/uploads/advs/' . $imageName;
            $adv->save();

        }
        return redirect('/webadmin/advs')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم تعديل الاعلان بنجاح']));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Adv::destroy($id);
        return redirect('/webadmin/advs')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم حذف الاعلان بنجاح']));

    }
}
