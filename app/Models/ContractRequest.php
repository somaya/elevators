<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class ContractRequest extends Model
{
    protected $table = 'contracts';
    protected $guarded = [];

    public $timestamps = true;
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function choice()
    {
        return $this->belongsTo(Choice::class, 'choice_id');
    }
}
