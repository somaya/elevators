<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Choice extends Model
{
    protected $table = 'choices';
    protected $guarded = [];

    public $timestamps = true;

    public function products()
    {
        return $this->belongsToMany(Product::class, 'choice_products','choice_id','product_id');
    }
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'branch_id');
    }
}
