<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Choice_product extends Model
{
    protected $table = 'choice_products';
    protected $guarded = [];

    public $timestamps = true;

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

}
